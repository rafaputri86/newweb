
from django.contrib import admin
from django.urls import path
from .views import *

urlpatterns = [
    path('', index),
    path('experience', experience),
    path('profile', profile),
    path('contact', contact),
]
