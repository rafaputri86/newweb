from django.shortcuts import render

def index(request):
	return render(request, 'index.html')

def profile(request):
    return render(request, 'profile.html')

def experience(request):
    return render(request, 'experience.html')

def contact(request):
    return render(request, 'contact.html')


